variable "target_resource_group" {
  type = string
  default = "poc-aks"
}

variable "target_location" {
  type = string
  default = "centralus"
}

# ---------------------------------------------------------------------------
# AKS
# ---------------------------------------------------------------------------

variable "aks_cluster_name" {
  type = string
  default = "poc-aks"
}

variable "aks_cluster_dns_prefix" {
  type = string
  default = "poc-aks"
}

variable "aks_cluster_network_plugin" {
  type = string
  default = "kubenet"
}

variable "aks_cluster_loadbalancer_sku" {
  type = string
  default = "Standard"
}

variable "aks_cluster_kubernetes_version" {
  type = string
  default = "1.20.7"
}


variable "aks_main_pool_node_type" {
  type = string
  default = "standard_a2_v2"
}

variable "aks_node_count" {
  type = number
  default = 1
}

variable "aks_main_pool_autoscaling_min_nodes" {
  type = number
  default = 1
}

variable "aks_main_pool_autoscaling_max_nodes" {
  type = number
  default = 2
}

variable "aks_cluster_service_principal_id" {
  type = string
}

variable "aks_cluster_service_principal_secret" {
  type = string
}

variable "aks_nodepool_name" {
  type = string
  default = "defaultpool"
}

variable "aks_enable_autoscale" {
  type = bool
  default = false
}

# ---------------------------------------------------------------------------
# VNET
# ---------------------------------------------------------------------------

variable "vnet_address_space" {
  type = string
  default = "172.12.0.0/16"
}

variable "vnet_subnet_space" {
  type = string
  default = "172.12.0.0/23"
}