provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "main" {
  name = var.target_resource_group
  location = var.target_location
}

resource "azurerm_virtual_network" "main_vnet" {
  name                = "${var.aks_cluster_name}-vnet-main"
  address_space       = [var.vnet_address_space]
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name
}

resource "azurerm_subnet" "main_subnet" {
  name                 = "${var.aks_cluster_name}-subnet-main"
  virtual_network_name = azurerm_virtual_network.main_vnet.name
  resource_group_name  = azurerm_resource_group.main.name
  address_prefixes     = [var.vnet_subnet_space]
}

resource "azurerm_kubernetes_cluster" "main" {
  name = var.aks_cluster_name
  location = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name
  dns_prefix = var.aks_cluster_dns_prefix
  kubernetes_version = var.aks_cluster_kubernetes_version


  default_node_pool {
    name                = var.aks_nodepool_name
    enable_auto_scaling = var.aks_enable_autoscale
    node_count          = var.aks_node_count
    min_count           = var.aks_main_pool_autoscaling_min_nodes
    max_count           = var.aks_main_pool_autoscaling_max_nodes
    vm_size             = var.aks_main_pool_node_type
    vnet_subnet_id      = azurerm_subnet.main_subnet.id
  }

  service_principal {
    client_id = var.aks_cluster_service_principal_id
    client_secret = var.aks_cluster_service_principal_secret
  }

  network_profile {
    network_plugin = var.aks_cluster_network_plugin
    load_balancer_sku = var.aks_cluster_loadbalancer_sku
  }
}