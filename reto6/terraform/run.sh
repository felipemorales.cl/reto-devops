#!/bin/bash


# TERRAFORM AZURE CREDENTIALS
export ARM_TENANT_ID=${AZURE_TENANT}
export ARM_SUBSCRIPTION_ID=${AZURE_SUBSCRIPTION}
export ARM_CLIENT_ID=${AZURE_CLIENT}
export ARM_CLIENT_SECRET=${AZURE_SECRET}
export TF_VAR_aks_cluster_service_principal_id=${AZURE_CLIENT}
export TF_VAR_aks_cluster_service_principal_secret=${AZURE_SECRET}

terraform init
terraform plan -out=plan.out
terraform apply -auto-approve plan.out