## Documentacion de los retos

# Reto 1 - Crear Dockerfile

La aplicación se crea en un contenedor de node.
Como usuario de despliegue utilizamos node, que es el usuario recomendado por Node. También esta la posibilidad de crear un usuario distinto.

```bash
$ cd reto1/app
$ docker build -t app .
```

Levante el servicio
```bash
$ docker run -p 3000:3000 app
```

Valide el servicio
```bash
$ curl http://localhost:3000
$ curl http://localhost:3000/public
$ curl http://localhost:3000/private
```

# Reto 2 - Docker Compose

El proyecto se expone en 2 puertos:
- 8080: Sin SSL
- 8443: Con SSL

## Archivo Compose
Dejamos 2 carpetas:
- reto2/app: Contiene los archivos del proyecto
- reto2/nginx: Agregamos la configuración de nginx para servir el proyecto.

## Certificado SSL

Creamos un certificado autofirmado con el comando:
```bash
$ cd ngnix/ssl
$ openssl req -x509 -nodes -days 3650 -newkey rsa:2048 -keyout localhost.key -out localhost.crt -config config -subj "/C=CL/ST=Metropolitan/L=Santiago/O=SRE/CN=localhost"
```

Los certificados quedaron en la carpeta: **nginx/ssl/**

**Basic-Auth**: Cree un archivo en la raiz del proyecto *nginx*, llamado *node-admin* que guarda el usuario autorizado:

```
user: admin
pass: admin
```

## Validación del Servicio

Levante el servicio
```bash
$ cd reto2
$ docker compose up --build
```

Valide el servicio
```bash
$ curl --insecure https://localhost:8443
{"msg":"ApiRest prueba"}

$ curl --insecure https://localhost:8443/public
{"public_token":"12837asd98a7sasd97a9sd7"}

$ curl --insecure https://localhost:8443/private
<html>
<head><title>401 Authorization Required</title></head>
<body>
<center><h1>401 Authorization Required</h1></center>
<hr><center>nginx/1.21.1</center>
</body>
</html>

$ curl --insecure -u admin:admin https://localhost:8443/private
{"private_token":"TWFudGVuIGxhIENsYW1hIHZhcyBtdXkgYmllbgo="}
```

Cualquier otra ruta (8080 para http) dara error por el redireccionamiento al 8443
```bash
$ curl http://localhost:8080/public
<html>
<head><title>301 Moved Permanently</title></head>
<body>
<center><h1>301 Moved Permanently</h1></center>
<hr><center>nginx/1.21.1</center>
</body>
</html>
```

# Reto 3 - Creación de un Pipeline con GitlabCI

Agregamos un pipeline que referencia a la carpeta **"reto3"**.

El pipeline lo separé en 2 etapas:
- build: Generamos la aplicación node
- docker: Guardamos la imagen en un contenedor del proyecto, uno para la app en node y otro para el nginx

Puede revisar las imagenes generadas en:
https://gitlab.com/felipemorales.cl/reto-devops/container_registry

# Reto 4 - Conexión con k8s

Modificamos el **pipeline** para agregar una etapa con el despliegue a K8s
- deploy: despliegue de las imágenes a Kubernetes

Opte por crear un cluster de K8s en DigitalOcean, para que el despliegue fuera más real, desde ahí obtengo un *KUBE_CONFIG* que queda como variable del proyecto y permite conectar con k8s.

En el cluster de k8s se desplegó un LoadBalancer con la IP pública **138.197.231.187**, en los puertos 8080 (http) y 8443 (https).
Dado que tiene una redirección del puerto 8080 al 8443 y que esta con un certificado autofirmado solo se puede usar de la siguiente forma:

```bash
$ curl --insecure -s https://138.197.231.187:8443
{
  "msg": "ApiRest prueba"
}
$ curl --insecure -s https://138.197.231.187:8443/public
{
  "public_token": "12837asd98a7sasd97a9sd7"
}
$ curl --insecure -u admin:admin -s https://138.197.231.187:8443/private
{
  "private_token": "TWFudGVuIGxhIENsYW1hIHZhcyBtdXkgYmllbgo="
}
```

Otros curl, por ejemplo al puerto **8080**, responderan con redirect. O si lo accede por navegador un mensaje de *alerta de privacidad*.

# Reto 6 - Infraestructura

Como conozco más la nube de Azure, agregue un proyecto de AKS (Azure Kubernetes) en terraform. Requiere de tener una cuenta en Azure. Deje todos los archivos relacionados en la carpeta **reto6**

Este proyecto se ejecuta con *docker*, mediante una imagen que aplica terraform para crear la infraestructura.

Para ejecutar el proyecto de infraestructura modifique el archivo **reto6/terraform/.env** agregando las siguientes variables:
```
AZURE_TENANT=
AZURE_SUBSCRIPTION=
AZURE_CLIENT=
AZURE_SECRET=
```

Luego ejecute el proyecto docker:
```bash
$ cd reto6/terraform
$ docker build -t aks .
$ docker run --rm --env-file=.env aks
```

Esto creara un cluster AKS en Azure, de 1 nodo, en el grupo de recursos "poc-aks".

**Importante**: No se implemento el RBAC solo la infraestructura (para que no la busquen).